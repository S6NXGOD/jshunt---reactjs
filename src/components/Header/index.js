import React from 'react';

import './styles.css';

const Header = () =>(
  <header id="main-header">ApiHunter</header>
);

export default Header;